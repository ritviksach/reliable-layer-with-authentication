<?xml version="1.0" encoding="US-ASCII"?>
<!-- This template is modified by Seth Nielson for creating P-RFC's
    (Playground-RFC's). -->
<!-- This template is for creating an Internet Draft using xml2rfc,
    which is available here: http://xml2rfc.ietf.org. -->
<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!-- One method to get references from the online citation libraries.
    There has to be one entity for each item to be referenced. 
    An alternate method (rfc include) is described in the references. -->

<!ENTITY RFC2119 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY RFC2360 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2360.xml">
<!ENTITY RFC2629 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2629.xml">
<!ENTITY RFC3552 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.3552.xml">
<!ENTITY RFC5226 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5226.xml">
]>
<?xml-stylesheet type='text/xsl' href='rfc2629.xslt' ?>
<!-- used by XSLT processors -->
<!-- For a complete list and description of processing instructions (PIs), 
    please see http://xml2rfc.ietf.org/authoring/README.html. -->
<!-- Below are generally applicable Processing Instructions (PIs) that most I-Ds might want to use.
    (Here they are set differently than their defaults in xml2rfc v1.32) -->
<?rfc strict="yes" ?>
<!-- give errors regarding ID-nits and DTD validation -->
<!-- control the table of contents (ToC) -->
<?rfc toc="yes"?>
<!-- generate a ToC -->
<?rfc tocdepth="4"?>
<!-- the number of levels of subsections in ToC. default: 3 -->
<!-- control references -->
<?rfc symrefs="yes"?>
<!-- use symbolic references tags, i.e, [RFC2119] instead of [1] -->
<?rfc sortrefs="yes" ?>
<!-- sort the reference entries alphabetically -->
<!-- control vertical white space 
    (using these PIs as follows is recommended by the RFC Editor) -->
<?rfc compact="yes" ?>
<!-- do not start each main section on a new page -->
<?rfc subcompact="no" ?>
<!-- keep one blank line between list items -->
<!-- end of list of popular I-D processing instructions -->




<rfc category="std" docName="rfc1" ipr="playgroundSpring201604" number="1">
 <!-- category values: std, bcp, info, exp, and historic
    ipr values: trust200902, noModificationTrust200902, noDerivativesTrust200902,
       or pre5378Trust200902, playgroundWinter201501
    you can add the attributes updates="NNNN" and obsoletes="NNNN" 
    they will automatically be output with "(if approved)" -->

 <!-- ***** FRONT MATTER ***** -->

 <front>
   <!-- The abbreviated title is used in the page header - it is only necessary if the 
        full title is longer than 39 characters -->

   <title abbrev="PRFC's">Playground Request For Comments: KISS Protocol</title>

   <!-- add 'role="editor"' below for the editors if appropriate -->

   <!-- Another author who claims to be an editor -->

   <author fullname="Purushottam Kulkarni" initials="P.K." role="editor"
           surname="Kulkarni">
     <organization>JHU Network Security Fall 2016</organization>

     <address>
       <postal>
         <street>JHU 160 Malone Hall/3400 North Charles St.</street>

         <!-- Reorder these if your country does things differently -->

         <city>Baltimore</city>

         <region>MD</region>

         <code>21218</code>

         <country>USA</country>
       </postal>

       <email>pkulkar6@jhu.edu</email>
	<!-- uri and facsimile elements may also be added -->
     </address>
   </author>


<author fullname="Ritvik Sachdev" initials="R.S." role="editor"
           surname="Sachdev">

     <organization>JHU Network Security Fall 2016</organization>

     <address>
       <postal>
         <street>JHU 160 Malone Hall/3400 North Charles St.</street>

         <!-- Reorder these if your country does things differently -->

         <city>Baltimore</city>

         <region>MD</region>

         <code>21218</code>

         <country>USA</country>
       </postal>

       <email>rsachde4@jhu.edu</email>
	<!-- uri and facsimile elements may also be added -->
     </address>
   </author>

<author fullname="Praveen Malhan" initials="P.M." role="editor"
           surname="Malhan">

     <organization>JHU Network Security Fall 2016</organization>

     <address>
       <postal>
         <street>JHU 160 Malone Hall/3400 North Charles St.</street>

         <!-- Reorder these if your country does things differently -->

         <city>Baltimore</city>

         <region>MD</region>

         <code>21218</code>

         <country>USA</country>
       </postal>

       <email>pmalhan1@jhu.edu</email>

	<!-- uri and facsimile elements may also be added -->
     </address>
   </author>

<author fullname="Vedasagar Karthykeyan" initials="V.K." role="editor"
           surname="Karthykeyan">
     <organization>JHU Network Security Fall 2016</organization>

     <address>
       <postal>
         <street>JHU 160 Malone Hall/3400 North Charles St.</street>

         <!-- Reorder these if your country does things differently -->

         <city>Baltimore</city>

         <region>MD</region>

         <code>21218</code>

         <country>USA</country>
       </postal>

       <email>vkarthy1@jhu.edu</email>
	 <!-- uri and facsimile elements may also be added -->
     </address>
   </author>

   <date year="2016" />

   <!-- If the month and year are both specified and are the current ones, xml2rfc will fill 
        in the current day for you. If only the current year is specified, xml2rfc will fill 
	 in the current day and month for you. If the year is not the current one, it is 
	 necessary to specify at least a month (xml2rfc assumes day="1" if not specified for the 
	 purpose of calculating the expiry date).  With drafts it is normally sufficient to 
	 specify just the year. -->

   <!-- Meta-data Declarations -->

   <area>General</area>

   <workgroup>Playground Experiment Director</workgroup>

   <!-- WG name at the upperleft corner of the doc,
        IETF is fine for individual submissions.  
	 If this element is not present, the default is "Network Working Group",
        which is used by the RFC Editor as a nod to the history of the IETF. -->

   <keyword>PRFC</keyword>

   <!-- Keywords will be incorporated into HTML output
        files in a meta tag but they have no effect on text or nroff
        output. If you submit your draft to the RFC Editor, the
        keywords will be used for the search engine. -->

   <abstract>
     <t>This document specifies Version 1.0 of the KISS Layer protocol.  
	The protocol provides communications security over playground.  
	The protocol allows client/server applications to communicate in a way that 
	is designed to prevent eavesdropping, or message forgery by providing confidentiality.</t>
   </abstract>
 </front>

 <middle>
   <section title="Introduction">
     <t>The primary goal of the KISS protocol is to provide confidentiality between two communicating applications. Layered on top of some 		reliable transport protocol (e.g., RIP), the Protocol provides connection security by enforcing privacy.  Symmetric Key cryptography is 	used for data encryption (e.g., AES).  The keys for this symmetric encryption are generated uniquely for each connection and are based on 		certificates exchanged by the underlying RIP protocol. </t>

     <t>The primary goal of the KISS protocol is Cryptographic security. 
	KISS should be used to establish a secure connection between two parties.</t>
   </section>

   <section title="Implementation">
       <t>To provide complete privacy and confidentiality between both the communicating parties, we will be using ephemeral keys. Whenever a new 		connection is established, the lower RIP Stack will complete the handshake protocol and both the parties will store certificates of each 		other in a certificate chain. The KISS protocol will obtain the Public Key Playground Address Pair from the lower protocol which consists 		of the certificate chain and also the private key of the host. The public key of the peer extracted from the certificates will be used to 		encrypt and send the Key and Initialization Vector (IV). The private key will be used to decrypt the Key and IV received from the peer on 		the other side.</t>

       <t>After receiving the required certificates and private key from the RIP Stack, a new key and Initialization Vector (IV) is generated and 		encrypted using the receiver's public key and sent across. The receiver decrypts the key with the private key obtained from the lower 		protocol and further communication can be done using the keys to encrypt data. The receiver performs the same operation and sends back a 		Key and IV encrypted with the sender's public key and the sender decrypts it using his private key. 
	</t>

	<t>This provides privacy as well as perfect forward secrecy. Even though the communication is happening using the symmetric keys, the keys 		will be encrypted and decrypted using Public Key Infrastructure. 
	</t>


       <t>To make the communication of the keys even more secure, the server and the client, both, will have a read key and a write key. The key 	sent by the client to the server will serve as the write key for the client and read key for the server and vice versa. This means that the 		client encrypts all the data he sends with his write key and decrypts messages received from server with the server's write key. The same 		happens in case of the server. Even if a key is compromised on one side, the whole connection/session is not compromised. </t>
		
    </section>

	<section title="Specifications">
		<section title="Encryption Algorithm">
			<t> The algorithm used to encrypt/decrypt the data received from the higher layer is AES (Advanced Encryption Standard) in 				Counter (CTR) mode. This is a block cipher mode of operation using counters for encryption. The counter is any function 			which produces a sequence and incremented for each block which is guaranteed not to repeat for a long time. The counter is 				encrypted using the secret write key and the value obtained is XORed with the plaintext to generate the ciphertext.</t>
	</section>
		<section title="Key Size">
			<t>
			The key size that is used is 256 bit (32 bytes). The IV (16 bytes) is generated randomly and also sent encrypted before 			the communication starts with the secret symmetric key. 
			</t>
		</section>	
	</section>

   
   <section title="Handshake Protocol">
	 <figure>
                <artwork><![CDATA[
    		C -----------------------------> S
		{Kc, IVc}

		C <----------------------------- S
					{Ks, IVs}
	
	                     Figure 1

		]]></artwork>
	</figure>


       <t>Kc here serves as the write key for client. Client uses this key and IVc to encrypt the data. At the server side, Kc will be used to 		decrypt the data sent by the client and hence this becomes the read key for the server.
	Ks serves as the write key for the server and uses it and IVs to encrypt the data. Client will decrypt the data using this key hence 		making it the read key for the client.</t>
   </section>

    
    <section title="Message Definitions">
    <t>Two different message definitions will be used for communication. One of them for exchanging keys and the other one for sending data. The 	message identifier for exchanging keys is known as KissHandshake and the definition is as below:</t>

	<t>PLAYGROUND_IDENTIFIER = "KissHandshake"</t>
                <t>MESSAGE_VERSION = "1.0"</t>
                <t>BODY = [
                    <list><t>[("Key", STRING),</t>
                        <t>("IV", STRING),</t>
            </list>
        </t>
	<t>]</t>


	<t>The message identifier for exchanging data is known as KissData and the definition is as below:</t>
	<t>PLAYGROUND_IDENTIFIER = "KissData"</t>
                <t>MESSAGE_VERSION = "1.0"</t>
                <t>BODY = [
                    <list><t>[("Data", STRING),</t>
            </list>
        </t>
	<t>]</t>

    </section>
</middle>
           

</rfc>
