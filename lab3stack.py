from lab2stack import RIPClientFactory, RIPServerFactory
from KISS import KISSFactory

def KISSClientFactory():
    kissfactory = KISSFactory
    ripFactory = RIPClientFactory
    return ripFactory.StackType(kissfactory)

def KISSServerFactory():
    kissfactory = KISSFactory
    ripFactory = RIPServerFactory
    return ripFactory.StackType(kissfactory)

ConnectFactory = KISSClientFactory()
ListenFactory = KISSServerFactory()
