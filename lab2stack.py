from twisted.internet import task, reactor
from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING, UINT1, UINT2, UINT4, BOOL1, LIST, DEFAULT_VALUE, OPTIONAL
from playground.network.common.Protocol import StackingTransport, StackingProtocolMixin, StackingFactoryMixin, MessageStorage
from playground.network.common.statemachine import StateMachine
from playground.crypto import PkiPlaygroundAddressPair
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from playground.crypto import X509Certificate
from Crypto.Hash import SHA256
import os
import random
import collections
import math
import CertFactory
import time

'''
    GLOBAL CONFIGURATION VARIABLES
'''

# Maximum segment size for data transmission in bytes
MSS = 8192

# Retransmission timeout period for a message in seconds
RETRANSMIT_PERIOD = 4

class RIPControlBlock():
    
    def __init__(self):
        self.nextSeqN = 0
        self.prevSeqN = 0
        self.nextAckN = 0
        self.signature = ""
        self.nonce = 0
        self.sessionID = ""
        self.acknowledgement_flag = False
        self.close_flag = False
        self.sequence_number_notification_flag = False
        self.reset_flag = False
        self.nonce = None
        self.peerNonce = None
        self.peerIPCert = None
        self.peerIPBlockCert = None
        
        self.retransmitBuff = {}
        self.incomingBuffer = {}
        self.lastAckNSent = 0
        
        self.pendingDataLC = None
        self.rtLC = None
        

class RIPMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIP.RIPMessageID"
    MESSAGE_VERSION = "1.0"
    
    BODY = [ 
        ("sequence_number", UINT4),
        ("acknowledgement_number", UINT4, OPTIONAL),
        ("signature", STRING, DEFAULT_VALUE("")),
        ("certificate", LIST(STRING), OPTIONAL),
        ("sessionID", STRING, DEFAULT_VALUE("")),
        ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
        ("close_flag", BOOL1, DEFAULT_VALUE(False)),
        ("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
        ("reset_flag", BOOL1, DEFAULT_VALUE(False)),
        ("data", STRING, DEFAULT_VALUE("")),
        ("OPTIONS", LIST(STRING), OPTIONAL)
    ]
    
    
class RIPTransport(StackingTransport):
    
    def __init__(self, lowerTransport, protocol):
        self.protocol = protocol
        StackingTransport.__init__(self, lowerTransport)
    
    def write(self, messageData):
        # Block writing of data from higher layer if in close requested/received state
        #if (self.protocol.fsm.currentState() == self.protocol.STATE_CLOSE_REQUESTED or self.protocol.fsm.currentState() == self.protocol.STATE_CLOSE_RECEIVED):
        #    print "Blocking write"
        #    return
        
        segmentList = [messageData[i:i + MSS] for i in range(0, len(messageData), MSS)]
        for msg in segmentList:
            messagePacket = RIPMessage()
            messagePacket.sequence_number = self.protocol.rcb.nextSeqN
            # messagePacket.acknowledgement_number = self.protocol.rcb.nextAckN
            messagePacket.sessionID = self.protocol.rcb.sessionID
            messagePacket.data = msg

            messagePacket.signature = ""
            messagePacket.signature = self.generatePacketSignature(messagePacket)
            self.protocol.debug("Sending data with seqN = " + str(self.protocol.rcb.nextSeqN))
            self.protocol.rcb.retransmitBuff[messagePacket.sequence_number] = [messagePacket, int(time.time())]
            self.lowerTransport().write(messagePacket.__serialize__())

            # Update sequence number
            self.protocol.rcb.prevSeqN = self.protocol.rcb.nextSeqN
            self.protocol.rcb.nextSeqN += len(msg)
            
            
        if (self.protocol.rcb.rtLC == None):
            self.protocol.rcb.rtLC = task.LoopingCall(self.retransmitPackets)
            self.protocol.rcb.rtLC.start((RETRANSMIT_PERIOD))    
            
        
    def retransmitPackets(self):
        if (len(self.protocol.rcb.retransmitBuff) == 0):
            return
        keyList = self.protocol.rcb.retransmitBuff.keys()
        keyList.sort()
        
        counter = 0
        for sequenceNumber in keyList:
            counter += 1
            currentTime = int(time.time())
            values = self.protocol.rcb.retransmitBuff[sequenceNumber]
            if (currentTime - values[1] > RETRANSMIT_PERIOD):
                self.protocol.debug("Retransmitting packet with seqn - " + str(sequenceNumber))
                packetToRetransmit = values[0]
                self.lowerTransport().write(packetToRetransmit.__serialize__())
                self.protocol.rcb.retransmitBuff[sequenceNumber][1] = int(time.time())
            if (counter == 5):    
                break
            
    def loseConnection(self):
        # Don't try to lose connection again if peer has already started closing handshake
        if (self.protocol.fsm.currentState() != self.protocol.STATE_CLOSE_RECEIVED):
            self.protocol.fsm.signal(self.protocol.SIGNAL_CLOSE_REQUEST, RIPMessage())
        
    def generatePacketSignature(self, messagePacket):
        data = messagePacket.__serialize__()
        
        c = CertFactory.getPrivateKeyForAddr(str(self.lowerTransport().getHost().host))
        
        rsaKey = RSA.importKey(c)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        hasher = SHA256.new()
        hasher.update(data)
        signedBytes = rsaSigner.sign(hasher)
        
        return signedBytes
    
    
    def getHost(self):
        hostPair = self.lowerTransport().getHost()
        host = str(hostPair.host)
        certs = CertFactory.getCertsForAddr(host)
        ipCert = certs[0]
        ipBlockCert = certs[1]

        c1 = X509Certificate.loadPEM(ipCert)
        c2 = X509Certificate.loadPEM(ipBlockCert)

        chain = [c1, c2]
        key = RSA.importKey(CertFactory.getPrivateKeyForAddr(host))

        return PkiPlaygroundAddressPair.ConvertHostPair(hostPair, chain, key)

    def getPeer(self):
        ipCert = self.protocol.rcb.peerIPCert
        ipBlockCert = self.protocol.rcb.peerIPBlockCert
        c1 = X509Certificate.loadPEM(ipCert)
        c2 = X509Certificate.loadPEM(ipBlockCert)
        chain = [c1, c2]

        peer = self.lowerTransport().getPeer()
        
        return PkiPlaygroundAddressPair.ConvertPeerPair(peer, chain)
    
    
        
        
    
class RIPProtocol(StackingProtocolMixin, Protocol):
    STATE_CLOSED            = "RIP: connection is closed"
    STATE_CLOSE_REQUESTED   = "RIP: host has requested peer to close the connection"
    STATE_FIN_RECEIVED      = "RIP: both host and peer simultaneously requested to close the connection"
    STATE_CLOSE_RECEIVED    = "RIP: received a connection close request from peer"
    STATE_ESTABLISHED       = "RIP: connection is established and active"
    STATE_LISTEN            = "RIP: passively open - listening for incoming connections"
    STATE_SYN               = "RIP: actively opening connection"
    STATE_SYN_SENT          = "RIP: SYN sent to peer, waiting for response"
    STATE_SYN_RECEIVED      = "RIP: ACK of SYN sent received, finishing threeway handshake"
    
    SIGNAL_ACTIVE_OPEN              = "RIP: start an active-open connection (client)"
    SIGNAL_PASSIVE_OPEN             = "RIP: start a passive-open connection (server)"
    SIGNAL_SYN_SENT                 = "RIP: host has sent SYN to peer (begin threeway handshake)"
    SIGNAL_SYN_ACK_RECEIVED         = "RIP: host received a SYN-ACK for the SYN sent to peer (second step in threeway handshake)"
    SIGNAL_SYN_ACK_ACK_RECEIVED     = "RIP: peer received an ACK for SYN-ACK sent to the originating peer (final step in threeway handshake)"
    SIGNAL_SYN_RECEIVED             = "RIP: peer received initial SYN by a host peer (second step in threeway handshake)"
    SIGNAL_DATA_RECEIVED            = "RIP: regular data received by a peer"
    SIGNAL_CLOSE_REQUEST            = "RIP: host has requested peer to close the connection"
    SIGNAL_CLOSE_RECEIVE            = "RIP: peer has received host peer's connection close request"
    SIGNAL_FIN_RECEIVED             = "RIP: peer has received a FIN from host who wishes to close to connection"
    SIGNAL_FIN_ACK_SENT             = "RIP: peer has sent final ack for the host's FIN message"
    SIGNAL_FIN_ACK_RECEIVED         = "RIP: host that wishes to close the connection has received an acknowledgement for the previously sent FIN message"
    
    DEBUG = True   
    
    def __init__(self):
        # Handles packet deserialization for us internally and 
        # provides us with an iterator to go over all the 
        # deserialized packets available for use
        self.storage = MessageStorage()
        
        self.dataTransport = None
        
        self.nextSeqN = self.generateISN()
        self.nextAckN = 0
        
        self.rcb = RIPControlBlock()

        
        self.fsm = StateMachine("RIPStateMachine")
        self.fsm.addState(self.STATE_CLOSED, 
                          (self.SIGNAL_ACTIVE_OPEN, self.STATE_SYN),
                          (self.SIGNAL_PASSIVE_OPEN, self.STATE_LISTEN), 
                          (self.SIGNAL_FIN_ACK_SENT, self.STATE_CLOSED),
                          onEnter=self.closeConnection
                         )
        
        self.fsm.addState(self.STATE_SYN,
                          (self.SIGNAL_SYN_SENT, self.STATE_SYN_SENT),
                          onEnter=self.openConnection
                         )
        
        self.fsm.addState(self.STATE_SYN_SENT,
                          (self.SIGNAL_SYN_ACK_RECEIVED, self.STATE_ESTABLISHED),
                          onEnter=self.handleSYNSent,
                          onExit=self.handleSYNACKReceived
                         )
        
        self.fsm.addState(self.STATE_SYN_RECEIVED, 
                          (self.SIGNAL_SYN_ACK_ACK_RECEIVED, self.STATE_ESTABLISHED),
                          onEnter=self.handleSYNMessages
                         )
        
        self.fsm.addState(self.STATE_LISTEN, 
                          (self.SIGNAL_SYN_RECEIVED, self.STATE_SYN_RECEIVED),
                          onEnter=self.handleEnterListen
                         )
        
        self.fsm.addState(self.STATE_ESTABLISHED,
                          (self.SIGNAL_DATA_RECEIVED, self.STATE_ESTABLISHED),
                          (self.SIGNAL_CLOSE_REQUEST, self.STATE_CLOSE_REQUESTED),
                          (self.SIGNAL_CLOSE_RECEIVE, self.STATE_CLOSE_RECEIVED),
                          onEnter=self.handleConnectionEstablishedMessages
                         )
        
        self.fsm.addState(self.STATE_CLOSE_REQUESTED,
                          (self.SIGNAL_DATA_RECEIVED, self.STATE_CLOSE_REQUESTED),
                          (self.SIGNAL_FIN_RECEIVED, self.STATE_FIN_RECEIVED),
                          (self.SIGNAL_FIN_ACK_RECEIVED, self.STATE_CLOSED),
                          onEnter=self.handleCloseRequested)
        
        self.fsm.addState(self.STATE_CLOSE_RECEIVED,
                          (self.SIGNAL_DATA_RECEIVED, self.STATE_CLOSE_RECEIVED),
                          (self.SIGNAL_FIN_ACK_SENT, self.STATE_CLOSED),
                          onEnter=self.handleCloseReceived)
        
        self.fsm.addState(self.STATE_FIN_RECEIVED,
                          (self.SIGNAL_DATA_RECEIVED, self.STATE_FIN_RECEIVED),
                          (self.SIGNAL_FIN_ACK_SENT, self.STATE_CLOSED),
                          (self.SIGNAL_FIN_ACK_RECEIVED, self.STATE_CLOSED),
                          onEnter=self.handleFINReceived)
        
        # Start the state machine
        self.fsm.start(self.STATE_CLOSED)
        
    def connectionMade(self):
        # Signifies default opening state
        # Explains whether the protocol belongs to 
        # Connecter (Client) or Listener (Server)
        # Values: active-open | passive-open
        self.connectionType = self.factory.connectionType
        
        self.dataTransport = RIPTransport(self.transport, self)
        
        # Connection established as a connector, hence send SNN
        if (self.connectionType == "active-open"):
            self.fsm.signal(self.SIGNAL_ACTIVE_OPEN, "")
            
        elif (self.connectionType == "passive-open"):
            self.fsm.signal(self.SIGNAL_PASSIVE_OPEN, "")
    
    def dataReceived(self, data):
        # self.debug("Received data: " + str(len(data)))
        self.storage.update(data);
        
        for msg in self.storage.iterateMessages():
            # msg is a deserialized message of the kind RIPMessage
            msgType = self.parseReceivedMessage(msg)
            if (msgType):
                self.fsm.signal(msgType, msg)
                
            if(msgType == self.SIGNAL_SYN_ACK_RECEIVED or msgType == self.SIGNAL_SYN_ACK_ACK_RECEIVED):
                self.makeHigherConnection(self.dataTransport)
        
    def parseReceivedMessage(self, message):
        # certificate chain validation
        if (self.verifiedPacketOrigin(message) == False):
            self.debug("Packet authenticity/integrity could not be validated! Dropping message packet! " + str(message.sequence_number))
            return None
        
        currentState = self.fsm.currentState()
        # START - Connection establishment - HANDSHAKE states
        # check flag states, system states etc and process the message packet accordingly
        if (message.sequence_number_notification_flag == True):
            # Initial sequence SYN message
            if (currentState == self.STATE_LISTEN):
                # received an isn message
                return self.SIGNAL_SYN_RECEIVED
            elif (currentState == self.STATE_SYN_SENT):
                if (message.acknowledgement_flag == True):  # and message.ackN == self.nextSeqN - 1
                    # received a syn-ack message
                    return self.SIGNAL_SYN_ACK_RECEIVED
        # Last stage of threeway handshake - ACK of SYN-ACK is received
        if (currentState == self.STATE_SYN_RECEIVED and message.acknowledgement_flag == True): # and message.ackN == self.nextSeqN - 1
            return self.SIGNAL_SYN_ACK_ACK_RECEIVED
        # END - Connection establishment - HANDSHAKE states
        # handle connection closing
        if (currentState == self.STATE_ESTABLISHED and message.close_flag == True):
            return self.SIGNAL_CLOSE_RECEIVE
        
        if ((currentState == self.STATE_CLOSE_REQUESTED or currentState == self.STATE_FIN_RECEIVED) and message.close_flag == True and message.acknowledgement_flag == True):
            return self.SIGNAL_FIN_ACK_RECEIVED
        
        # In close requested state but received FIN from peer
        if (currentState == self.STATE_CLOSE_REQUESTED and message.close_flag == True):
            return self.SIGNAL_FIN_RECEIVED
            
        if (message.acknowledgement_flag == True and (currentState == self.STATE_ESTABLISHED or currentState == self.STATE_CLOSE_REQUESTED or currentState == self.STATE_CLOSE_RECEIVED or currentState == self.STATE_FIN_RECEIVED)):
            self.updateAcks(message)
            return None
            
        if (currentState == self.STATE_ESTABLISHED or currentState == self.STATE_CLOSE_REQUESTED or currentState == self.STATE_CLOSE_RECEIVED or currentState == self.STATE_FIN_RECEIVED):
            self.handleDataReceived(message)
            return None
            #return self.SIGNAL_DATA_RECEIVED
        
        self.debug('Dropped an invalid message packet!')
        return None
    
    def handleSYNMessages(self, signal, message):
        self.debug('received isn + syn, continuing handshake, replying with a syn-ack message')
        self.debug('waiting for final ack to complete handshake')
        self.rcb.nextSeqN = self.generateISN()
        # reply to a received SYN message
        messagePacket = RIPMessage()
        messagePacket.sequence_number = self.rcb.nextSeqN
        messagePacket.acknowledgement_number = message.sequence_number + 1
        messagePacket.sequence_number_notification_flag = True
        messagePacket.acknowledgement_flag = True
        
        nonce1 = int(message.certificate[0], 16)
        nonce2 = self.generateNonce()
        self.debug("Nonce2 is " + str(nonce2))
        
        certsChain = CertFactory.getCertsForAddr(str(self.dataTransport.lowerTransport().getHost().host))
        ipCert = certsChain[0]
        ipBlockCert = certsChain[1]
            
        Nonce1Plus1 = self.intToNonce(nonce1 + 1)
        
        messagePacket.certificate = [str(nonce2), str(Nonce1Plus1), ipCert, ipBlockCert]
        
        messagePacket.signature = ""
        messagePacket.signature = self.signData(messagePacket.__serialize__())
        
        self.transport.write(messagePacket.__serialize__())
        
        # update rip control block
        self.rcb.nextSeqN = self.rcb.nextSeqN + 1
        self.rcb.nextAckN = message.sequence_number + 1
        self.rcb.sessionID = str(nonce2) + str(message.certificate[0]) # sessionID = Nonce2Nonce1
        self.rcb.nonce = nonce2
        self.rcb.peerNonce = nonce1
        self.rcb.peerIPCert = message.certificate[1]
        self.rcb.peerIPBlockCert = message.certificate[2]
        self.rcb.lastAckNSent = message.sequence_number + 1
        
    def handleSYNSent(self, signal, message):
        self.debug('syn message sent, switchted to syn-sent state, waiting for reply')
        
    def handleSYNACKReceived(self, signal, message):
        self.debug("syn-ack received, moving to established state, sending final ack to peer with seqN = " + str(self.rcb.nextSeqN))
        # connection initiator received syn-ack
        # send final ACK back to peer to finish threeway handshake
        messagePacket = RIPMessage()
        messagePacket.sequence_number = self.rcb.nextSeqN
        messagePacket.acknowledgement_number = message.sequence_number + 1
        messagePacket.acknowledgement_flag = True
        
        nonce2 = int(message.certificate[0], 16)
        Nonce1Plus1 = message.certificate[1]
        
        storedNonce = int(self.rcb.sessionID, 16)
        storedNoncePlus1 = self.intToNonce(storedNonce + 1)
        self.debug("Stored Nonce1 is " + str(storedNonce))
        self.debug("Got Nonce2 as " + str(nonce2))
        if (not str(Nonce1Plus1) == str(storedNoncePlus1)):
            self.debug("Nonce failed to match! Handshake failure! Should send RESET message - handleSYNACKReceived")
            return
        
        self.rcb.sessionID += str(message.certificate[0])
        Nonce2Plus1 = self.intToNonce(nonce2 + 1)
        
        messagePacket.certificate = [str(Nonce2Plus1)]
        
        messagePacket.sessionID = self.rcb.sessionID
        messagePacket.signature = ""
        messagePacket.signature = self.signData(messagePacket.__serialize__())
        
        self.transport.write(messagePacket.__serialize__())
        
        # update rip control block, sessionID is currently Nonce1
        self.rcb.nextSeqN += 1
        self.rcb.nextAckN = message.sequence_number + 1
        # self.rcb.sessionID += str(nonce2) # now sessionID = Nonce1Nonce2
        self.rcb.peerNonce = nonce2
        self.rcb.peerIPCert = message.certificate[2]
        self.rcb.peerIPBlockCert = message.certificate[3]
        self.rcb.lastAckNSent = message.sequence_number + 1
        
    def handleEnterListen(self, signal, message):
        self.debug('a passive open connection created, in listening mode')
    
    def handleConnectionEstablishedMessages(self, signal, message):
        if (signal == self.SIGNAL_SYN_ACK_RECEIVED):            
            self.debug('connection established for active peer')
            # make client's connection to higher layer
            # threeway handshake complete for active-open side, connection is established
            
        elif (signal == self.SIGNAL_SYN_ACK_ACK_RECEIVED):
            # verify nonce2 was correct, if not reset handshake
            Nonce2Plus1 = message.certificate[0]
            storedNonce = int(self.rcb.nonce, 16)
            storedNoncePlus1 = self.intToNonce(storedNonce + 1)
            
            if (not str(Nonce2Plus1) == str(storedNoncePlus1)):
                self.debug("Nonce failed to match! Handshake failure! Should send RESET message - handleConnectionEstablished")
                return

            self.rcb.nextAckN = message.sequence_number + 1
            self.rcb.lastAckNSent = message.sequence_number + 1
            # make server's connection to higher layer
            # threeway handshake complete for passive-open side as well            
            self.debug('connection established for passive peer')
            
        elif (signal == self.SIGNAL_DATA_RECEIVED):
            # self.debug("received data, passing to higher layer. Data: " + message.data + " ; Sequence Number: " + str(message.sequence_number))
            self.handleDataReceived(message)
            
    def handleCloseRequested(self, signal, message):
        if (signal == self.SIGNAL_DATA_RECEIVED):
            # Handle pending data received
            self.debug("Got pending data = " + str(message.sequence_number))
            self.handleDataReceived(message)
            return
        
        self.debug("Host has requested to close connection, sending FIN message to peer")
        self.debug("Waiting for acknowledgement of sent messages, acknowledging received messages")
        
        messagePacket = RIPMessage()
        messagePacket.sequence_number = self.rcb.nextSeqN
        messagePacket.sessionID = self.rcb.sessionID
        messagePacket.close_flag = True
        messagePacket.signature = self.signData(messagePacket.__serialize__())
        
        self.transport.write(messagePacket.__serialize__())
        self.rcb.prevSeqN = self.rcb.nextSeqN
        self.rcb.nextSeqN += 1
        
    def handleCloseReceived(self, signal, message):
        if (signal == self.SIGNAL_DATA_RECEIVED):
            # Handle pending data received
            self.debug("got pending data = " + str(message.sequence_number))
            self.handleDataReceived(message)
            return
        
        self.debug("Peer has requested to close connection")
        self.debug("Waiting for acknowledgement of sent messages, acknowledging received messages")
        if (self.rcb.pendingDataLC == None):
            # sendFinAck  sends an FIN-ACK if sent/received buffers are cleared
            self.rcb.pendingDataLC = task.LoopingCall(self.sendFinAck)
            self.rcb.pendingDataLC.start(0.1) # Check every 100 milliseconds if possible to send fin-ack
        
    def handleFINReceived(self, signal, message):
        if (signal == self.SIGNAL_DATA_RECEIVED):
            # Handle pending data received
            #self.debug("got pending data = " + str(message.sequence_number))
            self.handleDataReceived(message)
            return
        
        self.debug("Peer has requested to close connection")
        self.debug("Waiting for acknowledgement of sent messages, acknowledging received messages")
        if (self.rcb.pendingDataLC == None):
            # sendFinAck  sends an FIN-ACK if sent/received buffers are cleared
            self.rcb.pendingDataLC = task.LoopingCall(self.sendFinAck)
            self.rcb.pendingDataLC.start(0.1) # Check every 100 milliseconds if possible to send fin-ack
    
    def sendFinAck(self):
        if (len(self.rcb.retransmitBuff.keys()) != 0):
            # Data is still left to be acknowledged by peer, can not send fin ack
            return
        
        #if (len(self.rcb.incomingBuffer.keys()) != 0):
            # Acknowledgement of received data hasn't been sent yet, can not send fin ack
        #    return
        if (self.fsm.currentState() == self.STATE_CLOSED):
            # fin ack sent already, don't send it again
            # Clear the looping call
            if (self.rcb.pendingDataLC and self.rcb.pendingDataLC.running):
                self.rcb.pendingDataLC.stop()
                self.rcb.pendingDataLC = None
                
            if (self.rcb.rtLC and self.rcb.rtLC.running):
                self.rcb.rtLC.stop()
                self.rcb.rtLC = None
            return
        # Clear the looping call    
        if (self.rcb.pendingDataLC and self.rcb.pendingDataLC.running):
            self.rcb.pendingDataLC.stop()
            self.rcb.pendingDataLC = None
            
        self.debug("Sending FIN-ACK to peer")
        # Don't send fin ack until all pending data has been transmitted
        # sending a FIN ACK message
        messagePacket = RIPMessage()
        messagePacket.sequence_number = self.rcb.nextSeqN
        messagePacket.sessionID = self.rcb.sessionID
        # messagePacket.acknowledgement_number = ?
        messagePacket.acknowledgement_flag = True
        messagePacket.close_flag = True
        messagePacket.signature = self.signData(messagePacket.__serialize__())
        
        self.transport.write(messagePacket.__serialize__())
        self.rcb.nextSeqN += 1
        
        self.debug("FIN ACK message sent")
        # send kill switch later to ensure fin-ack delivery by reactor
        if (self.fsm.currentState() == self.STATE_CLOSE_RECEIVED or self.fsm.currentState() == self.STATE_FIN_RECEIVED):
            reactor.callLater(0.5, self.fsm.signal, self.SIGNAL_FIN_ACK_SENT, RIPMessage())
        #self.fsm.signal(self.SIGNAL_FIN_ACK_SENT, RIPMessage())
        
    def closeConnection(self, signal, message):
        # Clear the looping call
        if (self.rcb.pendingDataLC and self.rcb.pendingDataLC.running):
            self.rcb.pendingDataLC.stop()
            self.rcb.pendingDataLC = None
        
        if (self.rcb.rtLC and self.rcb.rtLC.running):
            self.rcb.rtLC.stop()
            self.rcb.rtLC = None
        self.pushRemainingData()
        
        # protocol has closed connection, now lose connection
        self.debug("Closing connection, Bye-bye!")
        self.higherProtocol().connectionLost(self.dataTransport)
        self.dataTransport.lowerTransport().loseConnection()
        #self.transport.loseConnection()
    
    def openConnection(self, signal, message):
        myIP = str(self.dataTransport.lowerTransport().getHost().host)
        certsChain = CertFactory.getCertsForAddr(myIP)
        
        self.debug('opening new connection')
        # establish a new connection by beginning threeway handshake
        self.rcb.nextSeqN = self.generateISN()
        messagePacket = RIPMessage()
        messagePacket.sequence_number = self.rcb.nextSeqN
        messagePacket.sequence_number_notification_flag = True
        
        ipCert = certsChain[0]
        ipBlockCert = certsChain[1]
        
        nonce1 = self.generateNonce()
        self.debug("Nonce1 is " + str(nonce1))
        messagePacket.certificate = [str(nonce1), ipCert, ipBlockCert]
        
        messagePacket.signature = ""
        messagePacket.signature = self.signData(messagePacket.__serialize__())
        
        self.transport.write(messagePacket.__serialize__())
        self.debug('switching to syn-sent state')
        self.fsm.signal(self.SIGNAL_SYN_SENT, RIPMessage())
        
        # update rip control block
        self.rcb.nextSeqN = self.rcb.nextSeqN + 1 # increment sequence number for future uses
        self.rcb.sessionID = str(nonce1) # sessionID = Nonce1 ; will be updated in syn
        self.rcb.nonce = nonce1
        
    def pushRemainingData(self):
        orderedIncomingBuffer = collections.OrderedDict(sorted(self.rcb.incomingBuffer.items()))
        for sequence_number in orderedIncomingBuffer:
            currentPacket = orderedIncomingBuffer[sequence_number]
            del self.rcb.incomingBuffer[sequence_number]
        
    def verifiedPacketOrigin(self, messagePacket):
        
        # perform certificate chain validation
        # three cases
            # opening handshake - verify certificates
            # closing handshake - verify certificates
            # data transmission - verify signature
            
        currentState = self.fsm.currentState()
        if (currentState == self.STATE_ESTABLISHED):
            # verify signature, return False if it doesn't match
            if (not self.verifiedSignature(messagePacket)):
                #self.debug("Failed signature check")
                return False
            
        # opening handshake states
        if (currentState == self.STATE_LISTEN or currentState == self.STATE_SYN_SENT):
            # verify certificates and signature
            if (not self.verifiedCertificates(messagePacket) or not self.verifiedSignature(messagePacket)):
                #self.debug("Failed certificate check")
                return False
        
        # in future - might have to verify certificates for closing also
        return True
    
    def verifiedSignature(self, messagePacket):
        
        signature = messagePacket.signature
        messagePacket.signature = ""
        
        cert = X509Certificate.loadPEM(self.rcb.peerIPCert)
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(messagePacket.__serialize__())
        result = rsaVerifier.verify(hasher, signature)
        
        return result
    
    def signData(self, data):
        
        data = str(data)
        
        c = CertFactory.getPrivateKeyForAddr(str(self.dataTransport.lowerTransport().getHost().host))
        
        rsaKey = RSA.importKey(c)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        hasher = SHA256.new()
        hasher.update(data)
        signedBytes = rsaSigner.sign(hasher)
        
        return signedBytes
    
    def verifySignedData(self, signedData, rawData, certificate):
        cert = X509Certificate.loadPEM(certificate)
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(rawData)
        result = rsaVerifier.verify(hasher, signedData)
        
        return result
    
    def verifiedCertificates(self, messagePacket):
        Certificates = messagePacket.certificate
        # Certificate[0] - nonce value
        # Certificate[1] - requesting ip certificate - 20164.1.1010.1 - sender
        # Certificate[2] - issuing block certificate - 20164.1.1010 - intermediate CA
        
        # Odd scenario, Certificate[1] contains signed nonce of peer, in that case values get pushed
        # We take care of that nonce in its respective function, ignore it here
        nonce = Certificates[0]
        if (len(Certificates) == 3):
            senderCert = X509Certificate.loadPEM(Certificates[1])
            intmCACert = X509Certificate.loadPEM(Certificates[2])
            self.rcb.peerIPCert = Certificates[1]
            self.rcb.peerIPBlockCert = Certificates[2]
        elif (len(Certificates) == 4):
            senderCert = X509Certificate.loadPEM(Certificates[2])
            intmCACert = X509Certificate.loadPEM(Certificates[3])
            self.rcb.peerIPCert = Certificates[2]
            self.rcb.peerIPBlockCert = Certificates[3]
        
        c = CertFactory.getRootCert()
        
        rootCert = X509Certificate.loadPEM(c)
        
        if (senderCert.getIssuer() != intmCACert.getSubject()):
            return False
        if (intmCACert.getIssuer() != rootCert.getSubject()):
            return False
        
        intmCAPkBytes = intmCACert.getPublicKeyBlob()
        intmCAPublicKey = RSA.importKey(intmCAPkBytes)
        rsaVerifier = PKCS1_v1_5.new(intmCAPublicKey)
        bytesToVerify = senderCert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rsaVerifier.verify(hasher, senderCert.getSignatureBlob()):
            return False
        
        rootPkBytes = rootCert.getPublicKeyBlob()
        rootPublicKey = RSA.importKey(rootPkBytes)
        rsaVerifier = PKCS1_v1_5.new(rootPublicKey)
        bytesToVerify = intmCACert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rsaVerifier.verify(hasher, intmCACert.getSignatureBlob()):
            return False
        
        return True
    
    
    def handleDataReceived(self, latestPacket):
        if (latestPacket.sequence_number in self.rcb.incomingBuffer or latestPacket.sequence_number < self.rcb.lastAckNSent):
            # print "Discarding duplicate packet - " + str(latestPacket.sequence_number)
            return
        
        self.debug("Got packet with sequence number: " + str(latestPacket.sequence_number))
        self.rcb.incomingBuffer[latestPacket.sequence_number] = latestPacket
        self.rcb.incomingBuffer = collections.OrderedDict(sorted(self.rcb.incomingBuffer.items()))

        self.sendAck()
        
    '''     
    def sendAck(self, latestPacket):
        safeToSendAckN = self.rcb.lastAckNSent
        expectedSequenceNumber = self.rcb.lastAckNSent
        if(latestPacket.sequence_number == expectedSequenceNumber):
            # send acknowledgement packet with acknowledgement_number = ackN + 1
            messagePacket = RIPMessage()
            messagePacket.sequence_number = self.rcb.prevSeqN
            messagePacket.acknowledgement_number = safeToSendAckN
            messagePacket.sessionID = self.rcb.sessionID
            messagePacket.acknowledgement_flag = True
            messagePacket.signature = ""
            messagePacket.signature = self.signData(messagePacket.__serialize__())

            self.higherProtocol() and self.higherProtocol().dataReceived(latestPacket.data)
            del self.rcb.incomingBuffer[latestPacket.sequence_number]
            
            self.transport.write(messagePacket.__serialize__())
            if (len(latestPacket.data) == 0):
                self.rcb.lastAckNSent = latestPacket.sequence_number + 1
            else:
                self.rcb.lastAckNSent = latestPacket.sequence_number + len(latestPacket.data)        
    ''' 
    def sendAck(self):
        safeToSendAckN = self.rcb.lastAckNSent
        
        expectedSequenceNumber = self.rcb.lastAckNSent
        for sequence_number in self.rcb.incomingBuffer.keys():                
            if (sequence_number != expectedSequenceNumber):
                self.debug("Packet drop detected! Waiting for packet to arrive. Expected = " + str(expectedSequenceNumber) + " but got instead = " + str(sequence_number))
                break
            
            currentPacket = self.rcb.incomingBuffer[sequence_number]
            safeToSendAckN = sequence_number + len(currentPacket.data)
            
            if (len(currentPacket.data) == 0):
                expectedSequenceNumber = sequence_number + 1
            else:
                expectedSequenceNumber = sequence_number + len(currentPacket.data)
            
            self.higherProtocol() and self.higherProtocol().dataReceived(currentPacket.data)
            del self.rcb.incomingBuffer[sequence_number]
        
        if (safeToSendAckN == self.rcb.lastAckNSent):
            return
        
        # send acknowledgement packet with acknowledgement_number = ackN + 1
        messagePacket = RIPMessage()
        messagePacket.sequence_number = self.rcb.prevSeqN
        messagePacket.acknowledgement_number = safeToSendAckN
        messagePacket.sessionID = self.rcb.sessionID
        messagePacket.acknowledgement_flag = True
        messagePacket.signature = ""
        messagePacket.signature = self.signData(messagePacket.__serialize__())

        # self.debug("Ack message SeqN is " + str(self.rcb.prevSeqN) + " Sending ackN = " + str(safeToSendAckN))

        self.transport.write(messagePacket.__serialize__())
        self.rcb.lastAckNSent = safeToSendAckN
            
    def updateAcks(self, message):
        self.debug("Got an acknowledgement message with ackN " + str(message.acknowledgement_number))
        ackSeqNum = message.acknowledgement_number
        
        for sequenceNumber in self.rcb.retransmitBuff.keys():
            if (sequenceNumber < ackSeqNum):
                #self.debug("Removing packet with sequence number " + str(sequenceNumber) + " from retransmission buffer")
                del self.rcb.retransmitBuff[sequenceNumber]
    
    def generateISN(self):
        return random.getrandbits(32)
    
    def generateNonce(self):
        return os.urandom(8).encode('hex')
    
    def intToNonce(self, i):
        h = hex(i)
        h = h[2:] # remove 0x
        if h[-1] == 'L':
            h = h[:-1] # remove "L"
        return h
    
    
    def debug(self, message):
        if(self.DEBUG):
            print "DEBUG(" + self.connectionType + "): " + message
        
class RIPConnectFactory(StackingFactoryMixin, Factory):
    protocol = RIPProtocol
    connectionType = "active-open"
        
class RIPListenFactory(StackingFactoryMixin, Factory):
    protocol = RIPProtocol
    connectionType = "passive-open"

ConnectFactory = RIPConnectFactory
ListenFactory = RIPListenFactory

