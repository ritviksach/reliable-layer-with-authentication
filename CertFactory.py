def getCertsForAddr(address):
    split = address.split(".")
    myIPBlock = split[0:3]
    myIPBlock = ".".join(myIPBlock)
    chain = []
    path = "Certificates/public-signed-" + address.replace(".", "-") + ".cert"
    
    with open(path) as f:
        chain.append(f.read())
    
    path = "Certificates/public-signed-" + myIPBlock.replace(".", "-") + ".cert"
    with open(path) as f:
        chain.append(f.read())
        
    return chain

def getPrivateKeyForAddr(address):
    path = "Certificates/private-" + address.replace(".", "-")
    with open(path) as f:
        key = f.read()
    return key

def getRootCert():
    path = "Certificates/public-signed-20164.cert"
    with open(path) as f:
        cert = f.read()
    return cert
